#include <stdio.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"



using namespace std;


__device__ long int GetLineIndexUrv(long int i, long int j, long int m)
{
	return j + m * i;
}

__device__ long int GetRowUrv(long int lineIndex, long int m)
{
	return lineIndex / m;
}

__device__ long int GetColumnUrv(long int lineIndex, long int m)
{
	return lineIndex % m;
}

__device__ long int GetMaxTidUrv(long int n, long int m)
{
	return n * m;
}

__device__ long int max_Urv(long int a, long int b)
{
	return a > b ? a : b;
}

__device__ long int min_Urv(long int a, long int b)
{
	return a < b ? a : b;
}

__device__ long int translateToBorder_Urv(long int border, long int i)
{
	return max_Urv(min_Urv(i, border - 1), 0);
}


__device__ double getElementUrv(double *a, int row, int i, int j)
{
	return a[j * row + i];
}

__global__ void PrewittOperator(double *a, unsigned char *b, long int row, long int column)
{
	/*
	int tensorY[3][3] = {							
						{-1, -1, -1},
						{0, 0, 0},
						{1, 1, 1}
					  };
	int tensorX[3][3] = {
						{-1, 0, 1},
						{-1, 0, 1},
						{-1, 0, 1}
					  };
	*/


	
	int tidX = threadIdx.x + blockIdx.x * blockDim.x;
	int tidY = threadIdx.y + blockIdx.y * blockDim.y;
	int dx = blockDim.x * gridDim.x;
	int dy = blockDim.y * gridDim.y;
	double G_x = 0;
	double G_y = 0;
	unsigned char value = 0;
	// double curValue = 0;
	// long int localTid = 0;
	double e1;
	double e2;
	double e3;
	double e4;
	// double e5;
	double e6;
	double e7;
	double e8;
	double e9;
	for (int i = tidX; i < row; i += dx)
	{
		for (int j = tidY; j < column; j += dy)
		{
			e1 = getElementUrv(a, row, translateToBorder_Urv(row, i - 1), translateToBorder_Urv(column, j - 1));
			e2 = getElementUrv(a, row, translateToBorder_Urv(row, i - 1), translateToBorder_Urv(column, j));
			e3 = getElementUrv(a, row, translateToBorder_Urv(row, i - 1), translateToBorder_Urv(column, j + 1));
			e4 = getElementUrv(a, row, translateToBorder_Urv(row, i), translateToBorder_Urv(column, j - 1));
			e6 = getElementUrv(a, row, translateToBorder_Urv(row, i), translateToBorder_Urv(column, j + 1));
			e7 = getElementUrv(a, row, translateToBorder_Urv(row, i + 1), translateToBorder_Urv(column, j - 1));
			e8 = getElementUrv(a, row, translateToBorder_Urv(row, i + 1), translateToBorder_Urv(column, j));
			e9 = getElementUrv(a, row, translateToBorder_Urv(row, i + 1), translateToBorder_Urv(column, j + 1));
			G_x = (e7 + e8 + e9) - (e1 + e2 + e3);
			G_y = (e3 + e6 + e9) - (e1 + e4 + e7);
			value = min_Urv(255, sqrt(G_x * G_x + G_y * G_y) );
			b[GetLineIndexUrv(j, i, row)] = value;
		}
	}

	return;
}
/**/
__host__ long int h_GetLineIndexUrv(long int i, long int j, long int m)
{
	return j + m * i;
}

double * ReadImageUrv(long int &row, long int &column, string inFile)
{
	FILE *in = fopen(inFile.c_str(), "rb");
	fread(&column, sizeof(int), 1, in);
	fread(&row, sizeof(int), 1, in);
	double *image = (double *) malloc(sizeof(double) * row * column);
	// long int value;
	unsigned char r;
	unsigned char g;
	unsigned char b;
	unsigned char a;
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
		{
			fread(&r, sizeof(unsigned char), 1, in);
			fread(&g, sizeof(unsigned char), 1, in);
			fread(&b, sizeof(unsigned char), 1, in);
			fread(&a, sizeof(unsigned char), 1, in);
			image[h_GetLineIndexUrv(j, i, row)] = 0.299 * r + 0.587 * g + 0.114 * b;
		}
	}
	fclose(in);
	return image;
}

void WriteImageUrv(long int row, long int column, unsigned char *image, string &outFile)
{
	unsigned char a;
	FILE *out;
	long int tid;

	out = fopen(outFile.c_str(), "wb");

	a = 0;
	fwrite(&column, sizeof(int), 1, out);
	fwrite(&row, sizeof(int), 1, out);


	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < column; j++)
		{
			tid = j * row + i;
			fwrite(&image[tid], sizeof(unsigned char), 1, out);
			fwrite(&image[tid], sizeof(unsigned char), 1, out);
			fwrite(&image[tid], sizeof(unsigned char), 1, out);
			fwrite(&a, sizeof(unsigned char), 1, out);
		}
	}

	fclose(out);
	return;
}

int main()
{
	double *a;
	unsigned char *b;
	double *dev_a;
	unsigned char *dev_b;
	long int n = 0;
	long int m = 0;
	// long int thread = 1;
	// long int blocks = 1;
	string inFile, outFile;
	cin >> inFile;
	cin >> outFile;
	a = ReadImageUrv(n, m, inFile);
	b = (unsigned char *) malloc(sizeof(unsigned char) * n * m);
	cudaMalloc(&dev_a, sizeof(double) * n * m);
	cudaMalloc(&dev_b, sizeof(unsigned char) * n * m);
	cudaMemcpy(dev_a, a, sizeof(double) * n * m, cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, sizeof(unsigned char) * n * m, cudaMemcpyHostToDevice);
	PrewittOperator << <dim3(20, 20), dim3(20, 20) >> >(dev_a, dev_b, n, m);
	cudaMemcpy(b, dev_b, sizeof(unsigned char) * n * m, cudaMemcpyDeviceToHost);
	WriteImageUrv(n, m, b, outFile);
	free(a);
	free(b);
	cudaFree(dev_a);
	cudaFree(dev_b);
    return 0;
}