#include "novikov.h"
#include "urvanov.h"
#include "diligul.h"
#include <string>

using namespace std;

void GenImage(long int n, long int m, string outFile)
{
	unsigned char a;
	unsigned char r;
	unsigned char g;
	unsigned char b;
	FILE *out;

	out = fopen(outFile.c_str(), "wb");

	a = 0;
	fwrite(&n, sizeof(int), 1, out);
	fwrite(&m, sizeof(int), 1, out);

	for (long int i = 0; i < n * m; ++i)
	{
		r = ((double)rand()) / RAND_MAX * 255;
		g = ((double)rand()) / RAND_MAX * 255;
		b = ((double)rand()) / RAND_MAX * 255;
		fwrite(&r, sizeof(unsigned char), 1, out);
		fwrite(&g, sizeof(unsigned char), 1, out);
		fwrite(&b, sizeof(unsigned char), 1, out);
		fwrite(&a, sizeof(unsigned char), 1, out);
	}
	fclose(out);
	return;
}

int Check(string etalon, string checked)
{
	unsigned char rChecked, rEtalon;
	unsigned char gChecked, gEtalon;
	unsigned char bChecked, bEtalon;
	unsigned char aChecked, aEtalon;
	long int n;
	long int m;
	FILE *inChecked;
	FILE *inEtalon;

	inEtalon = fopen(etalon.c_str(), "rb");
	inChecked = fopen(checked.c_str(), "rb");

	fread(&n, sizeof(int), 1, inChecked);
	fread(&m, sizeof(int), 1, inChecked);
	fread(&n, sizeof(int), 1, inEtalon);
	fread(&m, sizeof(int), 1, inEtalon);

	for (long int i = 0; i < n * m; ++i)
	{
		fread(&rEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&gEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&bEtalon, sizeof(unsigned char), 1, inEtalon);
		fread(&aEtalon, sizeof(unsigned char), 1, inEtalon);

		fread(&rChecked, sizeof(unsigned char), 1, inChecked);
		fread(&gChecked, sizeof(unsigned char), 1, inChecked);
		fread(&bChecked, sizeof(unsigned char), 1, inChecked);
		fread(&aChecked, sizeof(unsigned char), 1, inChecked);
		if (rChecked != rEtalon)
		{
			return 0;
		}
	}
	fclose(inChecked);
	fclose(inEtalon);
	return 1;
}

int main__()
{
	GenImage(1, 50, "1.data");
	cout << "Generated" << endl;
	main_diligul("1.data", "2_diligul.data");
	cout << "Diligul" << endl;
	main_diligulCPU("1.data", "2_diligulCPU.data");
	cout << "DiligulCPU" << endl;
	main_urvanov("1.data", "2_urvanov.data");
	cout << "Urvanov" << endl;
	cout << "Urvanov and Diligul:" << endl;
	cout << Check("2_diligulCPU.data", "2_urvanov.data") << endl;
	cout << "Diligul and DiligulCPU:" << endl;
	cout << Check("2_diligul.data", "2_diligulCPU.data") << endl;
	return 0;
}